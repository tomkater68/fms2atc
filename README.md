# fms2atc - Convert X-PLANE flightplans to 124thATC #

124thATC is a great ATC plugin for X-Plane 11. Importing a flightplan from the aircraft's FMC doesn't work with 737-800 ZiboMod and 
Toliss A3XX, because these aircrafts have their own FMC implementation. So I was looking for a convenient way to get the flightplan 
into the ATC Dialog without the need to enter all the waypoints. This little commandline tool reads the flightplan from FMS file, extracts 
the waypoints and writes them to the file "LASTFLIGHTPLAN.txt". If 124ATC is loaded then, this flightplan is shown in the ATC dialog and can 
easily be filed to ATC.
 
## Installation ##

No installation needed. Just extract the content of the zip file.

## Usage ##

If fms2atc is started without any commandline parameters, it searches "c:\X-PLANE 11\Output\FMS plans" for existing FMS files. The first file found
there is read an written to 124thATC. 

Pass the FMS file name to convert a certain FMS file:

*fms2atc EDDHEDDM01*

If X-PLANE is installed in a different directory, pass the X-PLANE root directory to the program:

*fms2atc -x <X-PLANE root dir>*

You can also set the cruising flight level, the airline name, the flightnumber and the transition flightlevel by passing one or more of the following parameters:

*fms2atc -l <cruising flightlevel (default: FL300)>*

*fms2atc -a <airline name (default: DLH)>*

*fms2atc -n <flightnumber (default: 666)>*

*fms2atc -t <transition flightlevel (default: FL50)>*

## Licence ##

This software is published under the terms of GNU GPL3
