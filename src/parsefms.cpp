/* Parse XPLANE FMS-File
Copyright (C) 2021  Michael Saalfeld <kvbuchhaltung@posteo.de>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "parsefms.h"

#include <QTextStream>
#include <QFile>
#include <QCoreApplication>
#include <QDir>

Parsefms::Parsefms()
{
}

void Parsefms::setFMSFile( const QString& fmsFile ) {

    mFMSFile = fmsFile;

    if ( !mFMSFile.endsWith( ".fms" ) )
        mFMSFile += ".fms";
};

void Parsefms::setXPlanePath( const QString& xPlanePath ) {

    mXPPath = xPlanePath;

    if ( !mXPPath.endsWith( "/" ) )
        mXPPath += "/";
};

void Parsefms::setFlightLevel( const int level ) {

    mFlightLevel = level;
}

void Parsefms::setAirline( const QString& airline ) {

    mAirline = airline;
}

void Parsefms::setFlightnumber( const int flightnumber ) {

    mFlightnumber = flightnumber;
}

void Parsefms::setTransitionLevel( const int transition ) {

    mTransitionLevel = transition;
}

QString Parsefms::findFirstFlightPlan() {

    console_out( QCoreApplication::translate( "parsefms", "\nNo flightplan passed. Looking for existing flightplans." ) );

    QDir dir( mXPPath + "Output/FMS plans/" );

    QStringList files = dir.entryList( QStringList() << "*.fms",QDir::Files );

    if ( files.size() ) {
        console_out( QCoreApplication::translate( "parsefms", "At least one flightplan found. Using first available flightplan." ) );
        return files.value( 0 );
    } else
        console_out( QCoreApplication::translate( "parsefms", "No flightplans found." ) );

    return QString();
}

void Parsefms::convertFile() {

    console_out( QCoreApplication::translate( "parsefms", "\nfms2ATC - Converting X-Plane flightplans to 124thATC." ) );
    console_out( QCoreApplication::translate( "parsefms", "\nX-Plane path: %1" ).arg( QDir::toNativeSeparators( mXPPath ) ) );

    if ( mFMSFile.isEmpty() )
        mFMSFile = findFirstFlightPlan();

    if ( mFMSFile.isEmpty() )
        return;

    if ( mFlightLevel < 10 || mFlightLevel > 400 ) {
        console_out( QCoreApplication::translate( "parsefms", "\nPassed invalid flightlevel. Setting default flightlevel (FL300)." ) );
        mFlightLevel = 300;
    }

    if ( mAirline.size() != 3 ) {
        console_out( QCoreApplication::translate( "parsefms", "\nPassed invalid airline code. Setting default airline (DLH)." ) );
        mAirline = "DLH";
    }

    if ( mFlightnumber < 0 || mFlightnumber > 9999 ) {
        console_out( QCoreApplication::translate( "parsefms", "\nPassed invalid flightnumber. Setting default flightnumber (666)." ) );
        mFlightnumber = 666;
    }

    if ( mTransitionLevel < 30 || mTransitionLevel > 180 ) {
        console_out( QCoreApplication::translate( "parsefms", "\nPassed invalid transition flightlevel. Setting default transition flightlevel (FL50)." ) );
        mTransitionLevel = 50;
    }

    QFile fms( mXPPath + "Output/FMS plans/" + mFMSFile );

    console_out( QCoreApplication::translate( "parsefms", "\nConverting X-Plane flightplan: %1." ).arg( mFMSFile) );

    if ( !fms.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
        console_out( QCoreApplication::translate( "parsefms", "\nUnable to open file: %1." ).arg( fms.fileName() ) );
        return;
    }

    QFile lastFlightPlan( mXPPath + "Resources/plugins/124thATC64/" + "LastFlightPlan.txt" );

    if (!lastFlightPlan.open(QIODevice::WriteOnly | QIODevice::Text) ) {
        console_out( QCoreApplication::translate( "parsefms", "\nUnable to open file: %1." ).arg( lastFlightPlan.fileName() ) );
        return;
    }

    QString flightplan = "FlightPlan#";

    QTextStream in( &fms );
    QTextStream out( &lastFlightPlan );

    while (!in.atEnd()) {
        QString line = in.readLine();

        //ignore unneeded information
        if ( line == "I" )
            continue;

        if ( line.contains( "Version" ) ) {
            QStringList values = line.split( " " );

            if ( values.size() != 2 ) {
                console_out( QCoreApplication::translate( "parsefms", "\nError reading flightplan. Version has wrong format." ) );
                return;
            }

            if ( values.at( 0 ) != "1100" ) {
                console_out( QCoreApplication::translate( "parsefms", "\nWrong version. Version must be 1100." ) );
            }
            continue;
        } //end if

        if ( line.contains( "CYCLE" ) )
            continue;

        if ( line.contains( "NUMENR" ) )
            continue;

        //set departure airport
        if ( line.startsWith( "ADEP" ) ) {
            QStringList values = line.split( " " );

            if ( values.size() != 2 ) {
                console_out( QCoreApplication::translate( "parsefms", "\nError reading flightplan. Departure airport has wrong format." ) );
                return;
            }

            out << QString( "DepartureICAO#%1").arg( values.at( 1 ) ) << "\n";

            continue;
        } //end if

        //set destination airport
        if ( line.startsWith( "ADES" ) ) {
            QStringList values = line.split( " " );

            if ( values.size() != 2 ) {
                console_out( QCoreApplication::translate( "parsefms", "\nError reading flightplan. Destination airport has wrong format." ) );
                return;
            }

            out << QString( "ArrivalICAO#%1").arg( values.at( 1 ) )  << "\n";
            out << QString( "FL#%1").arg( mFlightLevel) << "\n";

            continue;
        } //end if

        QStringList values = line.split( " " );

        if ( !values.size() ) {
            console_out( QCoreApplication::translate( "parsefms", "\nError reading flightplan. Wrong format." ) );
            return;
        }

        if ( values.at( 0 ) != "1" && values.at( 0 ) != "2" && values.at( 0 ) != "3" && values.at( 0 ) != "11" && values.at( 0 ) != "28" ) {
            console_out( QCoreApplication::translate( "parsefms", "\nError reading flightplan. Unknown waypoint type." ) );
            return;
        }

        //ignore departure and arrival airport
        if ( values.at( 0 ) == "1" )
            continue;

        //ignore unnamed waypoints
        if ( values.at( 0 ) == "28" )
            continue;

        flightplan += values.at( 1 ) + " ";

    } //end while

    out << flightplan << "\n";

    //set airline
    out << QString( "ICAOFlightName#%1" ).arg( mAirline) << "\n";

    //set flightnumber
    out << QString( "ICAOFlightNumber#%1" ).arg( mFlightnumber ) << "\n";

    //set transition level
    out << QString( "TransLevelFLDep#%1" ).arg( mTransitionLevel ) << "\n";
    out << QString( "TransLevelFLArr#%1" ).arg( mTransitionLevel ) << "\n";

    fms.close();
    lastFlightPlan.close();

    console_out( QCoreApplication::translate( "parsefms", "\nFlightplan successfully converted." ) );
}

void Parsefms::console_out( const QString& message ) {

    QTextStream out( stdout );
    out << message + "\n";
}
