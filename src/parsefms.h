/* Parse FMS file
Copyright (C) 2021  Michael Saalfeld <kvbuchhaltung@posteo.de>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef PARSEFMS_H
#define PARSEFMS_H

#include <QObject>

/*!\Parse FMS file */
/*!
  Reads FMS file created by SIMBRIEF, extracts waypoints and writes them to
  124thATC's LASTFLIGHTPLAN.txt
*/
class Parsefms
{
public:
    Parsefms(); ///<Constructor

    void setFMSFile( const QString& fmsFile ); ///<set FMS filename
    void setXPlanePath( const QString& xPlanePath ); ///<set XPlane root path
    void setFlightLevel( const int level ); ///<set cruising flightlevel
    void setAirline( const QString& airline ); ///<set airline IACO code
    void setFlightnumber( const int flightnumber ); ///<<set flightnumber
    void setTransitionLevel( const int transition ); ///<set transition level

    void convertFile(); ///<parse file and write waypoints to LASTFLIGHTPLAN.txt

private:
    QString mXPPath = QString(); ///<X-PLane root path
    QString mFMSFile = QString(); ///<FMS filename
    QString mAirline = QString(); ///<Airline code
    int mFlightLevel = 300; ///<Flightlevel
    int mFlightnumber = 666; ///<Flightnumber
    int mTransitionLevel = 50; ///<Transition flightlevel

    /*!
        Search flightplan
        Looks for FMS file in 'output/FMS plans'
        \return First FMS file in 'output/FMS plans'
    */
    QString findFirstFlightPlan();

    /*!
        Write message to console
        \param message message text
    */
    void console_out( const QString& message );
};

#endif // PARSEFMS_H
