/* main.cpp
Copyright (C) 2021  Michael Saalfeld <kvbuchhaltung@posteo.de>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <QCoreApplication>

#include <QCommandLineParser>
#include <QTextStream>
#include <QDir>

#include "parsefms.h"

#if _WIN32 || _WIN64
const QString XPPath = "c:/X-Plane 11/";
#else
const QString XPPath = QDir::homePath() + "/X-Plane 11/";
#endif

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName( "fms2ATC" );
    QCoreApplication::setApplicationVersion( "1.0" );

    QCommandLineParser cmdparser;

    cmdparser.setApplicationDescription( "Copy flightplan from X-Plane FMS Format to 124thATC" );
    cmdparser.addHelpOption();
    cmdparser.addVersionOption();
    cmdparser.addPositionalArgument( "flightplan", QCoreApplication::translate("main", "FMS file to convert" ) );

    // pass X-Plane root path -x
    QCommandLineOption rootOption( "x", QCoreApplication::translate("main", "X-Plane Path (default: %1").arg( QDir::toNativeSeparators( XPPath ) ), "root", XPPath );
    cmdparser.addOption( rootOption );

    // pass flightlevel -l
    QCommandLineOption levelOption( "l", QCoreApplication::translate("main", "Flightlevel (default: FL300)" ), "level", "300" );
    cmdparser.addOption( levelOption );

    // pass airline -a
    QCommandLineOption airlineOption( "a", QCoreApplication::translate("main", "Airline code (default: DLH)" ), "airline", "DLH" );
    cmdparser.addOption( airlineOption );

    // pass flightnummer -n
    QCommandLineOption numberOption( "n", QCoreApplication::translate("main", "Flightnumber (default: 666)" ), "number", "666" );
    cmdparser.addOption( numberOption );

    // pass transition flightlevel -t
    QCommandLineOption transitionOption( "t", QCoreApplication::translate("main", "Transition flightlevel (default: FL50)" ), "transition", "50" );
    cmdparser.addOption( transitionOption );

    cmdparser.process( a );

    const QStringList args = cmdparser.positionalArguments();

    Parsefms fmsparser;

    if ( args.size() )
        fmsparser.setFMSFile( args.at( 0 ) );

    fmsparser.setXPlanePath( cmdparser.value( rootOption ) );
    fmsparser.setFlightLevel( cmdparser.value( levelOption ).toInt() );
    fmsparser.setAirline( cmdparser.value( airlineOption ) );
    fmsparser.setFlightnumber( cmdparser.value( numberOption ).toInt() );
    fmsparser.setTransitionLevel( cmdparser.value( transitionOption ).toInt() );

    fmsparser.convertFile();

    return 0;
}
