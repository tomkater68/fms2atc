QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

RC_ICONS = fms2atc.ico

SOURCES += \
        main.cpp \
        parsefms.cpp

unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
macx:OBJECTS_DIR = ../build/o/mac

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    parsefms.h
